export interface Configuration {
    apiUrl: string;
    env: string;
    appName: string;
}


export enum Environment {
    prd = 'prd',
    stg = 'stg',
    dev = 'dev',
    Local = 'local',
}
  