export const config = {
    env: process.env['KAVAK_ENVIRONMENT'] || 'local',
    apiUrl: process.env['API_URL'] || 'http://0.0.0.0:8080',
    appName: process.env['DD_SERVICE']|| 'no workload name set in the environment',
    // use for the server only
    host: process.env['HOST'] || '0.0.0.0',
    port: process.env['PORT'] || '8080',
}