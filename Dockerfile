# stage 1. Install dependencies and build packages
# FROM node:16-bullseye-slim AS builder
FROM 306027853247.dkr.ecr.us-west-2.amazonaws.com/docker-node-web:1.0 AS builder

ARG ARTIFACTORY_ACCESS_TOKEN

# Temporary while the Kavak's base image is not ready.
RUN echo 'registry=https://kavak.jfrog.io/artifactory/api/npm/npm/' > ~/.npmrc \
    && echo '_authToken=${ARTIFACTORY_ACCESS_TOKEN}' >> ~/.npmrc \
    && echo 'always-auth=true' >> ~/.npmrc

WORKDIR /app
 
COPY package*.json ./

RUN npm i
RUN npm install -g @angular/cli@14.2.4
RUN npm install --save-dev @angular-devkit/build-angular@14.2.4

COPY . .
 
RUN npm run build:ssr

# stage 2 Install production dependencies and build final image
# FROM node:16-bullseye-slim
FROM 306027853247.dkr.ecr.us-west-2.amazonaws.com/docker-node-web:1.0

ARG ARTIFACTORY_ACCESS_TOKEN

COPY --from=builder /root/.npmrc /root/.npmrc

WORKDIR /app

COPY package*.json ./
RUN npm ci --only=production
COPY --from=builder /app/dist /app/dist

CMD ["node", "/app/dist/web-public/server/main.js"]

EXPOSE 8080
