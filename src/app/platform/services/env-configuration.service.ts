import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, shareReplay } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Configuration } from '../../../../config/config.interface';

@Injectable({
  providedIn: 'root'
})
export class EnvConfigurationService {
  private readonly apiUrl = environment.local ? 'http://localhost:4200' : 'http://0.0.0.0:8080';
  private configuration$!: Observable<Configuration>;
  constructor(private http: HttpClient) {}
  public load(): Observable<Configuration> {
    if (!this.configuration$) {
      this.configuration$ = this.http
        .get<Configuration>(`${this.apiUrl}/config`)
        .pipe(shareReplay(1));
    }
    return this.configuration$;
  }
}
