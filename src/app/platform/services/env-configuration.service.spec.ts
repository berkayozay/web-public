import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { EnvConfigurationService } from './env-configuration.service';
import { Observable } from 'rxjs';

describe('EnvConfigurationService', () => {
  let service: EnvConfigurationService;
  const mockHttpClient = {
    get: () => new Observable(subject => subject.next())
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(EnvConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
