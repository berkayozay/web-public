import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { AppComponent } from './app.component';
import { EnvConfigurationService } from './platform/services/env-configuration.service';
import { Configuration, Environment } from '../../config/config.interface';



describe('AppComponent', () => {
  let fakeEnvConfigurationService: Partial<EnvConfigurationService>;
  beforeEach(async () => {
    fakeEnvConfigurationService = {
      load: () => {
        return new Observable<Configuration>((subscriber) => {
          subscriber.next({
            appName: 'web-public',
            apiUrl: '',
            env: Environment.Local,
          })
          subscriber.complete()
        })
      }
    }

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      declarations: [
        AppComponent
      ],
      providers:[
        {
          provide: EnvConfigurationService,
          useValue: fakeEnvConfigurationService,
        }
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toBeTruthy();
  });

  it(`should have as title 'web-public'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('web-public');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.container h2')?.textContent).toContain('web-public');
  });
});
