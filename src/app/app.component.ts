import { Component } from '@angular/core';
import { tap } from 'rxjs';
import { EnvConfigurationService } from './platform/services/env-configuration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title!: string;
  environment!: string
  constructor(private envConfigurationService: EnvConfigurationService){
    this.envConfigurationService.load()
    .subscribe(config => {
      this.title = config.appName;
      this.environment = config.env;
    });
  }
}
