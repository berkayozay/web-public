import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: [
    '<rootDir>/setup-jest.ts'
  ],
  globalSetup: 'jest-preset-angular/global-setup',
  transform: {
    '^.+\\.(ts|mjs|js|html)$': 'jest-preset-angular',
  },
  transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
  collectCoverageFrom: [
    'src/**/*.ts', 
    '!**/node_modules/**', 
    '!src/main.ts', 
    '!src/**/polyfills*', 
    // '!src/*/*.module.ts', 
    '!src/**/*environment.*'],
  collectCoverage: true,
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  coverageDirectory: 'coverage/apps',
  coveragePathIgnorePatterns: [
    '/tests/',
    '.module.ts',
    '.server.ts'
  ],
  coverageThreshold: {
    global: {
      branches: 15,
      functions: 30,
      lines: 30,
      statements: 30,
    },
  },
  passWithNoTests: true,
  verbose: true,
  testTimeout: 20000,
}


export default config;
